#!/usr/bin/python

import sys

class MINE_INPUT :
  MINE = '*'
  EMPTY = '.'

class MATINDEX :
  ROW = 0
  COLUMN = 1
  MAX_MAT_DIM = 2

class MINERESULT :
  ERROR = 0
  SUCCESS = 1
  TERMINATE = 2

def isInteger(s):
  try:
    int(s)
    return True
  except ValueError:
    return False

def StringIsValid(s):
  ret = True
  for ch in s :
    if ch != MINE_INPUT.MINE and ch != MINE_INPUT.EMPTY :
      ret = False
      break

  return ret

class minesweeper :
  matrix_num = ""
  matrix_dim = [-1, -1]
  matrix_input = []
  result = []
  output_count = 0

  def initializer(self) :
    self.matrix_num = ""
    matrix_dim = [-1, -1]
    del self.matrix_input[:]
    del self.result[:]

  def __init__(self) :
    self.initializer()

  def handler(self) :
    while(1) :
      ret = self.get_matrix_num()
      if ret == MINERESULT.SUCCESS :
        if self.get_mine_matrix() == MINERESULT.SUCCESS :
          self.count_mine_map()
          self.print_mine_map()
          self.initializer()
        else :
          print "mine input is invalid"
          self.initializer()

      elif ret == MINERESULT.TERMINATE :
        break

      else :
        print "mine size is invalid (only from 0 to 100)"
        self.initializer()

  def get_mine_matrix (self) :
    ret = MINERESULT.SUCCESS
    for index in range(self.matrix_dim[MATINDEX.ROW]) :
      input_str = raw_input().strip()
      if len(input_str) == self.matrix_dim[MATINDEX.COLUMN] and StringIsValid(input_str) == True :
        self.matrix_input.append(input_str)
      else :
        ret = MINERESULT.ERROR
        break
    return ret

  def print_mine_map (self) :
    self.output_count += 1
    print '\nField #' + str(self.output_count) + ' :'
    for result_line in self.result :
      print "".join(result_line)

  def count_mine_point (self, row_p, column_p) :
    ret = 0
    for row_index in range(row_p -1 , row_p + 2) :
      for column_index in range(column_p -1 , column_p + 2) :
        if row_index >= 0 and column_index >= 0 and row_index < self.matrix_dim[MATINDEX.ROW] and column_index < self.matrix_dim[MATINDEX.COLUMN] :
          if row_index != row_p or column_index != column_p :
            map_ch = self.matrix_input[row_index][column_index]
            if map_ch == MINE_INPUT.MINE :
              ret += 1
    return ret

  def count_mine_map (self) :
    for row_index in range(self.matrix_dim[MATINDEX.ROW]) :
      row_list = []
      for column_index in range(self.matrix_dim[MATINDEX.COLUMN]) :
        map_ch = self.matrix_input[row_index][column_index]

        if map_ch == MINE_INPUT.EMPTY :
          result = self.count_mine_point(row_index, column_index)
          row_list.append(str(result))

        else :
          row_list.append(MINE_INPUT.MINE)
      self.result.append(row_list)

  def get_matrix_num(self) :
    ret = MINERESULT.ERROR
    matrix_num = raw_input()
    matrix_info = matrix_num.split()

    if(len(matrix_info) == MATINDEX.MAX_MAT_DIM) :
      for index, matrix_v in enumerate(matrix_info) :
        if(isInteger(matrix_v) == True) :
          mat_v = int(matrix_v)
          if((mat_v) >= 0 and (mat_v) <=100) :
            self.matrix_dim[index] = mat_v

    if self.matrix_dim[MATINDEX.ROW] == 0 and self.matrix_dim[MATINDEX.COLUMN] == 0 :
      ret = MINERESULT.TERMINATE

    elif self.matrix_dim[MATINDEX.ROW] != -1 and self.matrix_dim[MATINDEX.COLUMN] != -1 :
      ret = MINERESULT.SUCCESS

    return ret               

def main() :
   Mine = minesweeper()
   Mine.handler()

if __name__ == "__main__":
   main()