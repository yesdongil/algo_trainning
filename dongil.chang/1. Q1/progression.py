#!/usr/bin/python

import sys
import operator

def input_check_range(number) :
   ret = False
   if ( (number > 0) and (number < 1000000) ) :
      ret = True
   return ret

def input_exception_check(first_num, second_num) :
   ret = False
   if( (first_num <= second_num) and (input_check_range(first_num) == True) and (input_check_range(second_num) == True) ) :
      ret = True
   return ret

def get_cycle_length (number, count = 1) :
   if (number == 1) :
      return count

   elif (number % 2 == 0) :
      return get_cycle_length (number / 2, count + 1)
   else :
      return get_cycle_length ( ((number + 1)/2)*3 - 1, count + 2)

def main(argv, argc) :
   if (argc == 2) : 
      first_num = int(argv[0])
      second_num = int(argv[1])

      if (input_exception_check(first_num, second_num) == True) :
         result = {}
         for key_number in range(first_num, second_num + 1) :
            result[key_number] = get_cycle_length(key_number)
         print str(first_num) + ' ' + str(second_num) + ' ' + str(max(result.iteritems(), key=operator.itemgetter(1))[1])
      else :
         print "usage : tool [first_num] [second_num]\n(number shall be over 0 and 1000000 below)"
   else :
      print "usage : tool [first_num] [second_num]\n(number shall be over 0 and 1000000 below)"

if __name__ == "__main__":
   main(sys.argv[1:], len(sys.argv) - 1)
