#include <iostream>
#include <vector>
#include <string>
#include <math.h>
#include <limits>

using namespace std;

/* private type definitions */
typedef enum {
    E_ERROR,
    E_SUCCESS,
    E_TERMINATE
} result_t;

/* class definitions */

class expense_base {
  protected :
    uint32_t input_num;

  private :
    virtual result_t read_data(void) = 0;

  public :
    virtual void initialize(void) = 0;
    virtual result_t handler(expense_base* obj) = 0;
    virtual ~expense_base() {}

    uint32_t get_input_num(void) const {
        return input_num;
    }

    void set_input_num(uint32_t num) {
        input_num = num;
    }

};

class input_count : expense_base {
  private:
    enum {
        E_TERMINATE_INPUT = (0U),
        E_INPUT_MAX_NUM = (1000U)
    };

    virtual result_t read_data(void) {
        result_t ret = E_ERROR;
        uint32_t temp_num;

        cin >> temp_num;

        if (cin.fail() == false) {
            if (temp_num == E_TERMINATE_INPUT) {
                ret = E_TERMINATE;

            } else if (temp_num <= E_INPUT_MAX_NUM) {
                ret = E_SUCCESS;
                input_num = temp_num;

            } else {
                ;
            }
        }

        cin.clear();
        cin.ignore( numeric_limits <streamsize> ::max(), '\n' );
        return (ret);
    }

  public :
    virtual void initialize(void) {
        input_num = 0;
    }

    input_count() {
        initialize();
    }

    virtual result_t handler(expense_base* obj) {
        result_t result = E_ERROR;

        while (result == E_ERROR) {
            result = read_data();

            if (result == E_ERROR) {
                cout << "students number is invalid" << endl;
            }
        }

        return (result);
    }
};

class expense_process : expense_base {
  private:
    static const double  MAX_COST_INPUT;
    static const double  MIN_COST_INPUT;
    static const double  MIN_COST_DIFF;

    vector<double> expense_list;
    vector<double> cost_list;

    double caculate_equalized_cost(void) const;
    void print_equalized_result(double cost) const;

    virtual result_t read_data(void) {
        double expense_input;

        result_t ret = E_SUCCESS;
        uint32_t index = 0;

        for (; index < input_num; index++) {
            cin >> expense_input;

            if ((cin.fail() == false) && (expense_input <= MAX_COST_INPUT) && (expense_input >= MIN_COST_INPUT) ) {
                expense_list.push_back (expense_input);

            } else {
                ret = E_ERROR;
                break;
            }
        }

        cin.clear();
        cin.ignore( numeric_limits <streamsize> ::max(), '\n' );
        return (ret);
    }

  public :
    virtual void initialize(void) {
        expense_list.clear();
    }

    expense_process()  {
        cost_list.clear();
        initialize();
    }

    virtual result_t handler(expense_base* obj) {
        result_t result;
        uint32_t input_num = 0;
        input_num = obj->get_input_num();
        set_input_num(input_num);
        result = this->read_data();

        if (result == E_ERROR) {
            cout << "expense cost is invalid" << endl;

        } else if (result == E_SUCCESS) {
            print_equalized_result(caculate_equalized_cost());

        } else {
            ;
        }

        return (result);
    }
};

class main_handler {
  private :
    expense_process* expense_obj;
    input_count* input_count_obj;

  public :
    main_handler() {
        expense_obj = new expense_process;
        input_count_obj = new input_count;
    }

    ~main_handler() {
        delete expense_obj;
        delete input_count_obj;
    }

    void handler(void) const;
};

const double expense_process::MAX_COST_INPUT = (10000.00);
const double expense_process::MIN_COST_INPUT = (0.00);
const double expense_process::MIN_COST_DIFF = (0.01);

/* expense_process class member functions definitions */

double expense_process::caculate_equalized_cost(void) const {
    double average = 0.0, equalized_cost, temp_v;
    double negativeSum = 0, positiveSum = 0, diff;
    uint32_t count = 0;
    vector<double>::const_iterator it;

    for (it = expense_list.begin(); it != expense_list.end(); it++) {
        count++;
        average += *it;
    }

    average /= count;
    for (it = expense_list.begin(); it != expense_list.end(); it++) {
        temp_v = *it;
        diff = (double) (long) ((temp_v - average) * 100.0) / 100.0;

        if (diff < 0) {
            negativeSum += diff;

        } else {
            positiveSum += diff;
        }
    }

    equalized_cost	 = (-negativeSum > positiveSum) ? -negativeSum : positiveSum;
    return (equalized_cost);
}

void expense_process::print_equalized_result(double cost) const {
    cout << fixed;
    cout.setf(ios::showpoint);
    cout.precision(2);
    cout << "$" << cost << endl;
}

/* main_handler class member functions definitions */
void main_handler::handler(void) const {
    result_t result = E_SUCCESS;

    while (result != E_TERMINATE) {
        result = input_count_obj->handler(NULL);

        if(result == E_SUCCESS) {
            result = expense_obj->handler((expense_base*)input_count_obj);
        }

        input_count_obj->initialize();
        expense_obj->initialize();
    }
}

int main(int argc, char** argv) {
    main_handler trip_cost;
    trip_cost.handler();
    return 0;
}

