#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <stdint.h>
#include <cstring>

using namespace std;

/* private type definitions */
typedef enum {
    E_ERROR,
    E_SUCCESS,
    E_TERMINATE
} result_t;

/* class definitions */

class print_iterface {
  protected :
    uint8_t vertical_line;

  public :
    enum {
        E_PRINT_NOT_DONE = 0,
        E_PRINT_DONE = 1,
        E_PRINT_ERROR = 2,

    };

    enum {
        E_PRINT_SPACE = 0,
        E_PRINT_CH = 1,
        E_PRINT_MAX
    };

    virtual ~print_iterface() {}
    virtual uint8_t print_out(uint32_t segment_num, uint8_t print_ch) = 0;

    void inc_vertical_line(void) {
        vertical_line++;
    }

    void clear_vertical_line(void) {
        vertical_line = 0;
    }
};

class print_horizontal : print_iterface {
  public :
    virtual  uint8_t print_out(uint32_t segment_num, uint8_t print_ch) {
        static const uint8_t chracter_table[E_PRINT_MAX] = {
            ' ', '-'
        };

        uint8_t ch;
        uint32_t index = 0;
        cout << ' ';
        print_ch &= 0x1U;
        ch = chracter_table[print_ch];

        for(; index < segment_num; index++) {
            cout << ch;
        }

        cout << ' ';
        return E_PRINT_DONE;
    }
};

class print_leftside : print_iterface {
  public :
    virtual  uint8_t print_out(uint32_t segment_num, uint8_t print_ch) {
        static const uint8_t chracter_table[E_PRINT_MAX] = {
            ' ', '|'
        };

        uint8_t ret = E_PRINT_NOT_DONE;
        uint8_t ch;

        print_ch &= 0x1U;
        ch = chracter_table[print_ch];

        if(vertical_line < segment_num) {
            cout << ch;

            if(vertical_line == (segment_num - 1)) {
                ret = E_PRINT_DONE;
            }

        } else {
            ret = E_PRINT_ERROR;
        }

        return (ret);
    }
};

class print_rightside : print_iterface {
  public :
    virtual  uint8_t print_out(uint32_t segment_num, uint8_t print_ch) {
        static const uint8_t chracter_table[E_PRINT_MAX] = {
            ' ', '|'
        };

        uint32_t index = 0;
        uint8_t ret = E_PRINT_NOT_DONE;
        uint8_t ch;

        print_ch &= 0x1U;
        ch = chracter_table[print_ch];

        if(vertical_line < segment_num) {

            for(; index < segment_num; index++) {
                cout << ' ';
            }

            cout << ch;

            if(vertical_line == (segment_num - 1)) {
                ret = E_PRINT_DONE;
            }

        } else {
            ret = E_PRINT_ERROR;
        }

        return (ret);
    }
};

class print_lcd {
    typedef uint8_t (print_lcd::*print_out_t)(void);

  private:
    uint32_t segment_num;
    string displayed_num;

    enum {
        E_MAX_TABLE_NUM = 10
    };

    enum {
        E_TOP_HORIZONTAL_IDX = (0U),
        E_LEFT_UPPER_IDX = (1U),
        E_RIGHT_UPPER_IDX = (2U),
        E_MIDDLE_HORIZONTAL_IDX = (3U),
        E_LEFT_LOWER_IDX = (4U),
        E_RIGHT_LOWER_IDX = (5U),
        E_BOTTOM_HORIZONTAL_IDX = (6U),
        E_MAX_IDX
    };

    enum {
        E_TOP_HORIZONTAL = (1U << E_TOP_HORIZONTAL_IDX),
        E_LEFT_UPPER = (1U << E_LEFT_UPPER_IDX),
        E_RIGHT_UPPER = (1U << E_RIGHT_UPPER_IDX),
        E_MIDDLE_HORIZONTAL = (1U << E_MIDDLE_HORIZONTAL_IDX),
        E_LEFT_LOWER = (1U << E_LEFT_LOWER_IDX),
        E_RIGHT_LOWER = (1U << E_RIGHT_LOWER_IDX),
        E_BOTTOM_HORIZONTAL = (1U << E_BOTTOM_HORIZONTAL_IDX)
    };

    enum {
        E_HORIZON_PRINT_IDX = (0U),
        E_LEFT_PRINT_IDX = (1U),
        E_RIGHT_PRINT_IDX = (2U),
        E_MAX_PRINT_IDX
    };

    print_iterface* print_if[E_MAX_PRINT_IDX];

    static const char LCD_TABLE[E_MAX_TABLE_NUM];
    static const	uint8_t FUNC_TABLE[E_MAX_IDX];
    uint8_t print_1ch(char ch[], uint8_t count, uint8_t sequence) const;

  public :
    print_lcd() {
        print_if[E_HORIZON_PRINT_IDX] = (print_iterface*)(new print_horizontal);
        print_if[E_LEFT_PRINT_IDX] = (print_iterface*)(new print_leftside);
        print_if[E_RIGHT_PRINT_IDX] = (print_iterface*)(new print_rightside);
    }

    ~print_lcd() {
        delete print_if[E_HORIZON_PRINT_IDX];
        delete print_if[E_LEFT_PRINT_IDX];
        delete print_if[E_RIGHT_PRINT_IDX];
    }

    void set_print_lcd(uint32_t seg_num, string& display_num) {
        segment_num = seg_num;
        displayed_num = display_num;
    }

    void print_out(void) const;
};

class user_input_string {
  private :

    enum {
        E_MAXNUM_INPUT = (2U)
    };

    enum {
        E_TERMINATE_INPUT = (0U),
        E_MIN_SEGMENT = (1U),
        E_MAX_SEGMENT = (10U)
    };

    static const char SPACE_CHAR;
    vector<string> input_num;
    print_lcd	lcd_out;

    void split_string (istringstream& input, char delim);
    result_t validate_input (void);
    void initialize_data(void);

  public :

    enum {
        E_DISPLAY_MAX = (9U),
        E_DISPLAY_NUM_MAX = (999999999U)
    };

    result_t handler(void);

};

const char user_input_string::SPACE_CHAR = ' ';
const char print_lcd::LCD_TABLE[E_MAX_TABLE_NUM] = {
    (E_TOP_HORIZONTAL | E_LEFT_UPPER | E_RIGHT_UPPER | E_LEFT_LOWER | E_RIGHT_LOWER | E_BOTTOM_HORIZONTAL) /* 0 */,
    (E_RIGHT_UPPER | E_RIGHT_LOWER) /* 1 */,
    (E_TOP_HORIZONTAL | E_RIGHT_UPPER | E_MIDDLE_HORIZONTAL | E_LEFT_LOWER | E_BOTTOM_HORIZONTAL) /* 2 */,
    (E_TOP_HORIZONTAL | E_RIGHT_UPPER | E_MIDDLE_HORIZONTAL | E_RIGHT_LOWER | E_BOTTOM_HORIZONTAL) /* 3 */,
    (E_LEFT_UPPER | E_RIGHT_UPPER | E_MIDDLE_HORIZONTAL | E_RIGHT_LOWER) /* 4 */,
    (E_TOP_HORIZONTAL | E_LEFT_UPPER | E_MIDDLE_HORIZONTAL | E_RIGHT_LOWER | E_BOTTOM_HORIZONTAL) /* 5 */,
    (E_TOP_HORIZONTAL | E_LEFT_UPPER | E_MIDDLE_HORIZONTAL | E_LEFT_LOWER | E_RIGHT_LOWER | E_BOTTOM_HORIZONTAL) /* 6 */,
    (E_TOP_HORIZONTAL | E_RIGHT_UPPER | E_RIGHT_LOWER) /* 7 */,
    (E_TOP_HORIZONTAL | E_LEFT_UPPER | E_RIGHT_UPPER | E_MIDDLE_HORIZONTAL | E_LEFT_LOWER | E_RIGHT_LOWER | E_BOTTOM_HORIZONTAL) /* 8 */,
    (E_TOP_HORIZONTAL | E_LEFT_UPPER | E_RIGHT_UPPER | E_MIDDLE_HORIZONTAL | E_RIGHT_LOWER | E_BOTTOM_HORIZONTAL) /* 9 */
};

const uint8_t print_lcd::FUNC_TABLE[E_MAX_IDX] = {
    E_HORIZON_PRINT_IDX, /* E_TOP_HORIZONTAL_IDX = (0U) */
    E_LEFT_PRINT_IDX, /* E_RE_LEFT_UPPER_IDX = (1U) */
    E_RIGHT_PRINT_IDX, /* E_RIGHT_UPPER_IDX = (2U) */
    E_HORIZON_PRINT_IDX, /* E_MIDDLE_HORIZONTAL_IDX */
    E_LEFT_PRINT_IDX, /* E_RIGE_LEFT_LOWER_IDX */
    E_RIGHT_PRINT_IDX, /* E_RIGHT_LOWER_IDX */
    E_HORIZON_PRINT_IDX, /* E_BOTTOM_HORIZONTAL_IDX */
};

/* expense_process class member functions definitions */

result_t user_input_string::handler(void) {
    result_t ret = E_ERROR;
    uint32_t number;

    string input_string;
    getline( cin, input_string );
    istringstream stream(input_string);

    if (cin.fail() == false) {
        split_string(stream, SPACE_CHAR);
        ret = validate_input();

        if(ret == E_SUCCESS) {
            istringstream ( input_num.front() ) >> number;
            lcd_out.set_print_lcd(number, input_num.back());
            lcd_out.print_out();

        } else if(ret == E_ERROR) {
            cout << "input string error" << endl;
        }
    }

    initialize_data();
    cin.clear();
    return (ret);
}

void user_input_string::split_string (istringstream& input, char delim) {
    string token;

    while (getline(input, token, delim)) {
        input_num.push_back(token);
    }
}

result_t user_input_string::validate_input (void) {
    uint32_t number;
    result_t ret = E_ERROR;

    if (input_num.size() == E_MAXNUM_INPUT) {
        istringstream ( input_num.front() ) >> number;

        if ((number >= E_MIN_SEGMENT) && (number <= E_MAX_SEGMENT)) {
            string& ref_str = input_num.back();

            if (ref_str.length() <= E_DISPLAY_MAX) {
                istringstream (ref_str) >> number;

                if ((number >= 0) && (number <= E_DISPLAY_NUM_MAX)) {
                    ret = E_SUCCESS;
                }
            }

        } else if ((number == E_TERMINATE_INPUT) && (input_num.back() == "0") ) {
            ret = E_TERMINATE;

        } else {
            ;
        }

    } else if ( (input_num.size() == 1) && (input_num.front() == "0") ) {
        ret = E_TERMINATE;
    }

    return (ret);
}

void user_input_string::initialize_data(void) {
    input_num.clear();
}

void print_lcd::print_out(void) const {

    static const uint8_t print_table[] = {
        E_TOP_HORIZONTAL,
        (E_LEFT_UPPER | E_RIGHT_UPPER),
        E_MIDDLE_HORIZONTAL,
        (E_LEFT_LOWER | E_RIGHT_LOWER),
        E_BOTTOM_HORIZONTAL
    };

    char ch_array[user_input_string::E_DISPLAY_MAX + 1] = {0x00, };
    uint8_t count = 0, count_idx = 0, index = 0, ret = print_iterface::E_PRINT_DONE;

    count = displayed_num.length();
    strncpy(ch_array, displayed_num.c_str(), user_input_string::E_DISPLAY_MAX + 1);

    for(; index < count ; index++) {
        ch_array[index] -= 0x30;
    }

    for(index = 0; index < (sizeof(print_table) / sizeof(print_table[0])) ; index++) {
        do {
            ret = print_1ch(ch_array, count, print_table[index]);
        } while(ret == print_iterface::E_PRINT_NOT_DONE);
    }
}

uint8_t print_lcd::print_1ch(char ch[], uint8_t count, uint8_t sequence) const {
    uint8_t	ret = print_iterface::E_PRINT_DONE, print_ch;
    char table_info;
    char index = 0, temp_ch, seq_index, seq_bkup, seq_orig, seq_count = 0, print_index;

    for(; index < count; index ++) {
        temp_ch = ch[index];
        table_info = LCD_TABLE[temp_ch];
        seq_index = table_info & sequence;
        seq_orig = sequence;

        while ( seq_orig != false ) {
            seq_bkup = seq_orig;

            while ((seq_bkup & 0x1U) == false) {
                seq_count++;
                seq_bkup >>= 1;
            }

            print_index = FUNC_TABLE[seq_count];

            if ((seq_index & (0x1U << seq_count)) == false) {
                print_ch = false;

            } else {
                print_ch = true;
            }

            ret  = print_if[print_index]->print_out(segment_num, print_ch);

            seq_orig &= ~(0x1U << seq_count);
            seq_count = 0;
        }

        if (index < count - 1) {
            cout << ' ';
        }
    }

    cout << endl;

    if(ret == print_iterface::E_PRINT_NOT_DONE) {
        print_if[E_LEFT_PRINT_IDX]->inc_vertical_line();
        print_if[E_RIGHT_PRINT_IDX]->inc_vertical_line();

    } else {
        print_if[E_LEFT_PRINT_IDX]->clear_vertical_line();
        print_if[E_RIGHT_PRINT_IDX]->clear_vertical_line();

    }

    return (ret);
}

int main(int argc, char** argv) {
    result_t	 ret;
    user_input_string lcd_input;

    do {
        ret = lcd_input.handler();
    } while (ret != E_TERMINATE);

    return 0;
}
