/*
 * Printing LED problem
 * Solved By Badsaram (2005. 09. 06)
 */

//#define DEBUG

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* defines */
#define FALSE				0
#define TRUE				(!0)

#define MAX_SCALE			10
#define MAX_NUM				99999999
#define MAX_SEG				7
#define MAX_STR				9  /* length of MAX_NUM + 1('\0') */
#define MAX_LINE			5

#define INDEX_SEG_TOP		0
#define INDEX_SEG_LEFT_1	1
#define INDEX_SEG_RIGHT_1	2
#define INDEX_SEG_MIDDLE	3
#define INDEX_SEG_LEFT_2	4
#define INDEX_SEG_RIGHT_2	5
#define INDEX_SEG_BOTTOM	6

#define CHAR_HORIZON		'-'
#define CHAR_VERTICAL		'|'
#define CHAR_EMPTY			' '
#define CHAR_SPACE			' '

#define LINE_0				0 
#define LINE_1				1
#define LINE_2				2
#define LINE_3				3
#define LINE_4				4

#define IS_LEGAL_NUM(x)		( (0 <= (x)) && ((x) <= 99999999) )
#define IS_LEGAL_SCALE(x)	( (1 <= (x)) && ((x) <= 10) )

/* type definition */
struct sample_led_format
{
	char seg_7[MAX_SEG];
};

/* global variables */
static struct sample_led_format sample_led_data[] =  
/* sample led data. size = 1 */
{
	{ 1, 1, 1, 0, 1, 1, 1 }, /* 0 */
	{ 0, 0, 1, 0, 0, 1, 0 }, /* 1 */
	{ 1, 0, 1, 1, 1, 0, 1 }, /* 2 */
	{ 1, 0, 1, 1, 0, 1, 1 }, /* 3 */
	{ 0, 1, 1, 1, 0, 1, 0 }, /* 4 */
	{ 1, 1, 0, 1, 0, 1, 1 }, /* 5 */
	{ 1, 1, 0, 1, 1, 1, 1 }, /* 6 */
	{ 1, 1, 1, 0, 0, 1, 0 }, /* 7 */
	{ 1, 1, 1, 1, 1, 1, 1 }, /* 8 */
	{ 1, 1, 1, 1, 0, 1, 1 }, /* 9 */
};

/*
 * draw_horizon()
 *
 * draw horizon bar
 */
void draw_horizon(int index, char *str, int scale, int num)
{
	int i = 0, j = 0;		/* loop index */
	int str_len = 0;    	/* length of string */
	int sample_index = 0;	/* sample data index */

#ifdef DEBUG
	printf("[DEBUG] index : %d\n", index);
#endif

	switch (index)
	{
	case LINE_0:
		sample_index = 0;
		break;
	case LINE_2:
		sample_index = 3;
		break;
	case LINE_4:
		sample_index = 6;
		break;
	default:
		//ignore
		return;
	}
	
	str_len = strlen(str);

	for (i = 0; i < str_len; i++)
	{
		putchar(CHAR_SPACE);
		for (j = 0; j < scale; j++)
		{
			if (sample_led_data[str[i] - '0'].seg_7[sample_index] == 1)
			{
				putchar(CHAR_HORIZON);
			}
			else
			{
				putchar(CHAR_EMPTY);
			}
		}

		putchar(CHAR_SPACE); // space after horizon
		putchar(CHAR_SPACE); // gap between num and num
	}

	putchar('\n');
}

/*
 * draw_vertical()
 *
 * draw vertical
 */
void draw_vertical(int index, char *str, int scale, int num)
{
	int i = 0, j = 0, z = 0;/* loop index */
	int str_len = 0;		/* length of str */
	int sample_index = 0;	/* sample index */

#ifdef DEBUG
	printf("[DEBUG] index : %d\n", index);
#endif

	switch (index)
	{
		case LINE_1:
			sample_index = 1;
			break;
		case LINE_3:
			sample_index = 4;
			break;

		default:
			// ignore
			return;
	}

	str_len = strlen(str);

	for (i = 0; i < scale; i++)
	{
		for (j = 0; j < str_len; j++)
		{
			/* print left */
			if (sample_led_data[str[j] - '0'].seg_7[sample_index] == 1)
			{
				putchar(CHAR_VERTICAL);
			}
			else
			{
				putchar(CHAR_EMPTY);
			}

			/* print middle space */
			for (z = 0; z < scale; z++)
			{
				putchar(CHAR_SPACE);
			}

			/* print right */
			if (sample_led_data[str[j] - '0'].seg_7[sample_index + 1] == 1)
			{
				putchar(CHAR_VERTICAL);
			}
			else
			{
				putchar(CHAR_EMPTY);
			}

			/* print space after num */
			putchar(CHAR_SPACE);
		}

		putchar('\n'); 
	}
}

/*
 * print_num()
 *
 * it prints number :-)
 */
void print_number(int scale, int num)
{
	int i = 0;				/* loop index */
	char str[MAX_STR] = ""; /* converted number to string */

	/* fill str */
	memset(str, 0, sizeof(str));
	sprintf(str, "%d", num);

	/* 
	 * print_led
	 *
	 *  -
	 * | |
	 *  -
	 * | |
	 *  _
	 */
	for (i = 0; i < MAX_LINE; i++)
	{
		/* LINE_0, LINE_2, LINE_4 */
		if ((i % 2) == 0)
		{
			draw_horizon(i, str, scale, num);
		}
		else
		{
			draw_vertical(i, str, scale, num);
		}
	}
}

int main(void)
{
	int num = 0;		/* input number */
	int scale = 0;		/* scale of number */

	while (1)
	{
		/* input number & check */
		scanf("%d %d", &scale, &num);

		/* it means last input */
		if ((scale == 0) && (num == 0))
		{
			exit(0);
		}

		if ((IS_LEGAL_SCALE(scale) == FALSE) ||
			(IS_LEGAL_NUM(num) == FALSE))
		{
			printf("Not Legal Scale, Number. (scale : %d, num : %d)", scale, num);
			exit(0);
		}

#ifdef DEBUG
		printf("[DEBUG] scale : %d, num : %d\n", scale, num);
#endif

		/* print number */
		print_number(scale, num);
	}

	return 1;
}
