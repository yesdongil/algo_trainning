#include <stdio.h>
#include <string.h>

#define TRUE			(1)
#define FALSE   		(0)

#define ERROR			(-1)
#define OKAY		    (0)
#define END             (1)

#define MIN_MINE_BOARD	(0)
#define MAX_MINE_BOARD  (100)

#define MAX_INPUT_LEN   (MAX_MINE_BOARD + 1)

#define MINE_CHAR		'*'
#define NON_MINE_CHAR   '.'

#define MAX_MINE_XY 	(8)

int calc_mine_xy[MAX_MINE_XY][2] = {
	{ -1, -1 },
	{0, -1 },
	{1, -1 },
	{-1, 0 },
	{1, 0},
	{-1, 1},
	{0, 1},
	{1, 1}
};

static int is_in_range(int n, int num1, int num2)
{
	int min = 0, max = 0;

	if (num1 < num2)
	{
		min = num1;
		max = num2;
	}
	else
	{
		min = num2;
		max = num1;
	}

	if ((min <= n) && (n <= max))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static int check_line(char *input, int len, int mine_width)
{
	int i = 0;
	int input_len = 0;

	input_len = strlen(input);

	for (i = input_len - 1; i >= 0; i--)
	{
		if (isspace(input[i]))
		{
			input[i] = '\0';
			input_len--;
		}
	}

	if ((input_len == 0) || (input[0] == '\n'))
	{
		return EOF;
	}	

	if (input_len != mine_width)
	{
		return ERROR;
	}

	for (i = 0; i < mine_width; i++)
	{
		if ((input[i] != MINE_CHAR) && (input[i] != NON_MINE_CHAR))
		{
			return ERROR;
		}
	}

	return OKAY;
}

static int calc_mine_count(char mine_board[MAX_MINE_BOARD][MAX_MINE_BOARD], int x, int y, int w, int h)
{
	int i = 0;
	int count = 0;
	int newx = 0, newy = 0;

	for (i = 0; i < MAX_MINE_XY; i++)
	{
		newx = x + calc_mine_xy[i][0];
		newy = y + calc_mine_xy[i][1];

		if ((newx >= 0) && (newy >= 0) &&
		    (newx < w) && (newy < h))
		{
			if (mine_board[newy][newx] == MINE_CHAR)
			{
				count++;	
			}
		}
	}

	return count;
}

static void print_mine_board(int field, char mine_board[MAX_MINE_BOARD][MAX_MINE_BOARD], int m, int n)
{
	int i = 0, j = 0;
	int mine_count = 0;

	printf("\n");
	printf("field #%d\n", field);

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			if (mine_board[i][j] == MINE_CHAR)
			{
				printf("%c", mine_board[i][j]);
			}
			else
			{
				mine_count = calc_mine_count(mine_board, j, i, m, n);

				printf("%d", mine_count);
			}
		}

		printf("\n");
	}
}

static void insert_mine_board(char mine_board[MAX_MINE_BOARD][MAX_MINE_BOARD], char *input, int y, int w)
{
	int i = 0;

	for (i = 0; i < w; i++)
	{
		mine_board[y][i] = input[i];
	}
}

int main(int argc, char *argv[])
{
	int	 	line = 0;
	int		m = 0, n = 0;
	int 	check_line_ret = 0;
	int     field = 0;
	
	char 	input[MAX_INPUT_LEN] = { 0 };
	char    mine_board[MAX_MINE_BOARD][MAX_MINE_BOARD] = { 0 };

	while (TRUE)
	{
		scanf("%d %d", &m, &n);

		if ((m == 0) && (n == 0))
		{
			break;
		} 

		if (is_in_range(m, MIN_MINE_BOARD, MAX_MINE_BOARD) == FALSE)
		{
			fprintf(stderr, "wrong input range\n");

			continue;
		}

		if (is_in_range(n, MIN_MINE_BOARD, MAX_MINE_BOARD) == FALSE)
		{
			fprintf(stderr, "wrong input range\n");	

			continue;
		}

		fgets(input, MAX_INPUT_LEN, stdin); /* clear input buffer */

		for (line = 0; line < m; line++)
		{
			fgets(input, MAX_INPUT_LEN, stdin);

			check_line_ret = check_line(input, MAX_INPUT_LEN, m);
			switch (check_line_ret)
			{
			case END:
				break;

			case ERROR:
				fprintf(stderr, "wrong input character or string length\n");

				break;	

			case OKAY:
				insert_mine_board(mine_board, input, line, n);

				break;
			}		

			if (check_line_ret != OKAY)
			{
				break;
			}
		}

		if (line != m)
		{
			fprintf(stderr, "wrong input line %d (%d x %d)\n", line, m, n);
		}
		else
		{
			print_mine_board(field, mine_board, m, n);
		}
	}

	return 0;
}
