#include <stdio.h>
#include <string.h>

#define IS_EVEN(n)	(((n) % 2) == 0)
#define IS_ODD(n)	(((n) % 2) != 0)

static void swap(int *a, int *b)
{
	int temp = 0;

	temp = *a;
	*a = *b;
	*b = temp;
}

static int calc_cycle_length(int n)
{
	int cycle_length = 1;

	if ((n < 0) || (n > 1000000UL))
	{
		return -1;
	}

#if (DEBUG == 1)
	printf("[%d]\n", n);
#endif

	while (n != 1)
	{
		if (IS_EVEN(n))
		{
			n = n >> 1; /* n / 2 */
		}
		else
		{
			n = (3 * n) + 1; 
		}

		cycle_length++;

#if (DEBUG == 1)
		printf("\t%d\n", n);
#endif
	}

	return cycle_length;
}

int main(int argc, char *argv[])
{
	int i = 0;
	int first = 0, last = 0;
	int temp_cycle_length = 0, max_cycle_length = 0;

	if (argc != 3)
	{
		fprintf(stderr, "wrong argument number\n");
		fprintf(stderr, "3n_plus_1 <num1> <num2>\n");

		return -1;
	}

	first = atoi(argv[1]);
	last  = atoi(argv[2]);

	if ((first > 1000000UL) || (first < 0) || (last > 1000000UL) || (last < 0))
	{
		fprintf(stderr, "wrong input range. (0 ~ 1000000)\n");

		return -1;
	}

	if (first > last)
	{
		swap(&first, &last);
	}

	for (i = first; i <= last; i++)
	{
		temp_cycle_length = calc_cycle_length(i);

		if (temp_cycle_length > 0)
		{
			if (temp_cycle_length > max_cycle_length)
			{
				max_cycle_length = temp_cycle_length;
			}
		}
	}

	printf("%d %d %d\n", first, last, max_cycle_length);

	return 0;
}
