#include <stdio.h>
#include <stdlib.h>

//#define DEBUG

#define TRUE 			1
#define FALSE			0

#define MAX_MEMBERS		1000 
#define MAX_PAYMENT		10000.00f
#define MAX_STR			256

float g_payment[MAX_MEMBERS];

static float get_average_payment(int member_count, float *payment_list)
{
	int 	i = 0;
	float 	sum = 0;

	for (i = 0; i < member_count; i++)
	{
		sum += payment_list[i];
	}

	return (sum / member_count);
}

static void calc_and_print_member_payment(int member_count, float *payment_list)
{
	int 	i = 0;
	float 	average = 0;
	float   plus_payment_exchange = 0, minus_payment_exchange = 0;

	average = get_average_payment(member_count, payment_list);
	average = ((int)(average * 100)) / 100;
#ifdef DEBUG
	printf("average : %f\n", average);
#endif

	for (i = 0; i < member_count; i++)
	{
		if (payment_list[i] > average)
		{
			plus_payment_exchange += (payment_list[i] - average);
		}
		else
		{
			minus_payment_exchange += (average - payment_list[i]);
		}
	}

#ifdef DEBUG
	printf("plus_payment_exchange : %f\n", plus_payment_exchange);
	printf("minus_payment_exchange : %f\n", minus_payment_exchange);
#endif

	if (plus_payment_exchange < minus_payment_exchange)
	{
		printf("$%.2f\n", plus_payment_exchange);
	}
	else
	{
		printf("$%.2f\n", minus_payment_exchange);
	}
}

static void flush_stdin(void)
{
	char str[MAX_STR] = { 0 };

	fgets(str, MAX_STR, stdin);
}

int main(void)
{
	int 	member_count 	= 0;
	float 	member_payment 	= 0;

	int 	i = 0;

	while (TRUE)
	{
		scanf("%d", &member_count);

		if (member_count == 0)
		{
			return -1;
		}

		if ((member_count >  MAX_MEMBERS) || (member_count < 0))
		{
			fprintf(stderr, "wrong member count. exit..\n");
			return -1;
		}

		flush_stdin();

		for (i = 0; i < member_count; i++)
		{
			scanf("%f", &member_payment);

			if ((member_payment > MAX_PAYMENT) || (member_payment < 0))
			{
				fprintf(stderr, "wrong payment value. exit..\n");
			
				return -1;	
			}

			g_payment[i] = member_payment;

			flush_stdin();
		}

		calc_and_print_member_payment(member_count, g_payment);
	}

	return 0;
}
